import java.util.ArrayList;

public class Patient extends Person {
    private String patientID;
    private ArrayList<Illness> illnessList;

    public Patient(String name, int age, String socialSecurityNumber, String patientID, ArrayList<Illness> illnessList) {
        super(name, age, socialSecurityNumber);
        this.patientID = patientID;
        this.illnessList = illnessList;
    }

    public void addIllness(Illness illness) {
        illnessList.add(illness);
    }

    public String getInfo() {
        String illList = "";
        if(illnessList.isEmpty()) {
            illList = "No illness associated";
        } else {
            for(Illness illness : illnessList) {
                illList += illness.getName() + " ";
            }
        }
        return "Patient informations : \n Name : " + getName() + ",\n Age : " + getAge() + ",\n SSN : " + getSocialSecurityNumber() + ",\n Illnesses : " + illList;
    }
}
