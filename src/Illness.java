import java.util.ArrayList;

public class Illness {
    private String name;
    private ArrayList<Medication> medicationList;

    public Illness(String name, ArrayList<Medication> medicationList) {
        this.name = name;
        this.medicationList = medicationList;
    }

    public void addMedication(Medication medication) {
        medicationList.add(medication);
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        String medList = "";
        if(medicationList.isEmpty()) {
            medList = "No medication associated";
        } else {
            for(Medication medication : medicationList) {
                medList += medication.getInfo();
            }
        }
        return "Illness name : " + name + ", Medication associated : " + medList;
    }
}
