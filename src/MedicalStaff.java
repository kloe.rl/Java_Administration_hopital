public abstract class MedicalStaff extends Person implements Care {
    protected String employeeID;

    public MedicalStaff(String name, String employeeID) {
        super(name);
        this.employeeID = employeeID;
    }

    protected abstract String getRole();
}
