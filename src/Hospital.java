import java.util.ArrayList;

public class Hospital {
    public static void main(String[] args) {
        Illness flemmiteStandard = new Illness("Flemmite Standard", new ArrayList<>());
        Illness grippeArcEnCiel = new Illness("Grippe Arc-En-Ciel", new ArrayList<>());
        Illness catatonieDuPhasme = new Illness("Catatonie Du Phasme", new ArrayList<>());
        Medication padamalgam200 = new Medication("Padamalgam", "200mg");
        Medication tommeDeBrebisBienAffine = new Medication("Tomme De Brebis Bien Affiné", "250g");
        Medication poudreDePerlinpinpin = new Medication("Poudre De Perlinpinpin", "2 cuillères à soupe bien tassées");
        Patient titouan = new Patient("Titouan", 28, "9864477", "ID00012", new ArrayList<Illness>());
        Patient josue = new Patient("Josué", 51, "0A2291313", "ID002887", new ArrayList<Illness>());
        Doctor maroufle = new Doctor("Maroufle", "ID0045","Cuisiniste");
        Doctor pinpon = new Doctor("Pinpon", "ID0017", "Troubadour");
        Nurse farfalle = new Nurse("Farfalle", "ID0023");

        flemmiteStandard.addMedication(tommeDeBrebisBienAffine);
        grippeArcEnCiel.addMedication(padamalgam200);
        catatonieDuPhasme.addMedication((poudreDePerlinpinpin));
        titouan.addIllness(grippeArcEnCiel);
        titouan.addIllness(catatonieDuPhasme);
        josue.addIllness(flemmiteStandard);

        System.out.println(titouan.getInfo());
        System.out.println(josue.getInfo());

        System.out.println(pinpon.getRole());

        System.out.println(farfalle.careForPatient(titouan));
        System.out.println(maroufle.careForPatient(josue));

        System.out.println(tommeDeBrebisBienAffine.getInfo());
        System.out.println(catatonieDuPhasme.getInfo());
    }
}