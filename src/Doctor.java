public class Doctor extends MedicalStaff {
    private String specialty;

    public Doctor(String name, String employeeID, String specialty) {
        super(name, employeeID);
        this.specialty = specialty;
    }


    @Override
    public String getRole() {
        return "Doctor " + name + " is a well-known " + specialty ;
    }

    @Override
    public String careForPatient(Patient patient) {
        return "Doctor " + this.getName() + " cares for Patient " + patient.getName();
    }
}
