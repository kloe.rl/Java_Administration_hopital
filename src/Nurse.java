public class Nurse extends MedicalStaff {

    public Nurse(String name, String employeeID) {
        super(name, employeeID);
    }

    @Override
    protected String getRole() {
        return "Nurse";
    }

    @Override
    public String careForPatient(Patient patient) {
        return "Nurse " + this.getName() + " cares for Patient " + patient.getName();
    }
}
